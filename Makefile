REBAR := ./rebar -j8

.PHONY: all deps clean release test

all: compile

compile: deps
	$(REBAR) compile

deps:
	$(REBAR) get-deps

test:
	ERL_AFLAGS="-config ${PWD}/rel/files/app.test" $(REBAR)  compile ct suite=cbsweeper skip_deps=true

clean:
	$(REBAR) clean

relclean:
	rm -rf rel/cbsweeper

generate: compile
	cd rel && ../$(REBAR) generate

run: generate
	./rel/cbsweeper/bin/cbsweeper start

console: generate
	./rel/cbsweeper/bin/cbsweeper console

foreground: generate
	./rel/cbsweeper/bin/cbsweeper foreground

erl: compile
	erl -pa ebin/ -pa deps/*/ebin/ -s cbsweeper
