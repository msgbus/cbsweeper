%%%-------------------------------------------------------------------
%%% @author ZhengYinyong
%%% @copyright (C) 2015, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 10. 二月 2015 下午5:26
%%%-------------------------------------------------------------------
-module(cbsweeper_SUITE).
-author("ZhengYinyong").

-compile(export_all).

-include_lib("common_test/include/ct.hrl").
-include("cbsweeper.hrl").

-define(TEST_UID_DATA, [
    <<"2546328374358220281">>,
    <<"2546328374358220282">>,
    <<"2546328374358220283">>
]).

-define(TEST_MESSAGEID, <<"508891534553251840">>).

-define(TEST_HOST, "localhost").
-define(TEST_PORT, 6611).

init_per_suite(Config) ->
    {ok, _} = application:ensure_all_started(ranch),
    {ok, _} = application:ensure_all_started(poolboy),
    {ok, _} = application:ensure_all_started(cberl),
    {ok, _} = application:ensure_all_started(cbsweeper),

    {ok, Socket} = case gen_tcp:connect(?TEST_HOST, ?TEST_PORT, [binary, {packet, 0}, {active,false}]) of
                       {ok, Socket1} ->
                           ct:log("CONNECT ~p:~p successfully~n", [?TEST_HOST, ?TEST_PORT]),
                           {ok, Socket1};
                       {error, Reason} ->
                           ct:log("CONNECT ~p:~p failed beacause of ~p~n", [?TEST_HOST, ?TEST_PORT, Reason])
                   end,

    insert_test_data_in_route_table(Socket),
    insert_test_data_in_msg_queue(Socket),
    Config.

end_per_suite(_Config) ->
    ok.

all() ->
    [
        test_data_exist_in_cb_and_ets,
        test_sweep
    ].

test_data_exist_in_cb_and_ets(_Config) ->
    lists:foreach(fun(Uid) ->
        true = is_key_exist_in_cb(?ROUTE_TABLE_POOL, Uid),
        true = is_key_exist_in_ets(?ROUTE_TABLE_TIMESTAMP_INFO, Uid)
    end, ?TEST_UID_DATA),

    lists:foreach(fun(Uid) ->
        true = is_key_exist_in_cb(?MSG_QUEUE_POOL, << <<"msgq_">>/binary, Uid/binary >>),
        true = is_key_exist_in_ets(?MSG_QUEUE_TIMESTAMP_INFO, << <<"msgq_">>/binary, Uid/binary >>)
    end, ?TEST_UID_DATA).

test_sweep(_Config) ->
    cbsweeper_worker:sweep(1),
    lists:foreach(fun(Uid) ->
        false = is_key_exist_in_cb(?ROUTE_TABLE_POOL, Uid),
        false = is_key_exist_in_ets(?ROUTE_TABLE_TIMESTAMP_INFO, Uid)
    end, ?TEST_UID_DATA),

    lists:foreach(fun(Uid) ->
        false = is_key_exist_in_cb(?MSG_QUEUE_POOL, << <<"msgq_">>/binary, Uid/binary >>),
        false = is_key_exist_in_ets(?MSG_QUEUE_TIMESTAMP_INFO, << <<"msgq_">>/binary, Uid/binary >>)
    end, ?TEST_UID_DATA).

%%%===================================================================
%%% Internal functions
%%%===================================================================
insert_test_data_in_route_table(Socket) ->
    lists:foreach(fun(Uid) ->
                    cb_set(?ROUTE_TABLE_POOL, Uid, <<"dummy">>, 0),
                    send_tcp_test_data(Socket, route_table, Uid, cbsweeper_utils:get_millisec())
                  end, ?TEST_UID_DATA).

insert_test_data_in_msg_queue(Socket) ->
    lists:foreach(fun(Uid) ->
                    save_message_id_to_cb(<<"msgq_">>, Uid, ?TEST_MESSAGEID, 43200),
                    send_tcp_test_data(Socket, msg_queue, << <<"msgq_">>/binary, Uid/binary >>, cbsweeper_utils:get_millisec())
                  end, ?TEST_UID_DATA).

save_message_id_to_cb(PrefixBin, UidBin, MessageIdBin, TTL) ->
    MsgQKey = <<PrefixBin/binary, UidBin/binary>>,
    push_message_id_to_cb(MsgQKey, MessageIdBin, TTL).

push_message_id_to_cb(MsgQKeyBin, MessageIdBin, TTL) ->
    cb_lenqueue(?MSG_QUEUE_POOL, MsgQKeyBin, binary_to_integer(MessageIdBin)),
    case TTL of
        undefined ->
            ok;
        _ ->
            cberl:touch(?MSG_QUEUE_POOL, MsgQKeyBin, TTL)
    end.

cb_lenqueue(Pool, Key, Value) ->
    try
        cberl:lenqueue(Pool, Key, 0, Value),
        ok
    catch
        _:_X ->
            {error, _X}
    end.

cb_set(Pool, Key, Value, Exp) ->
    try
        cberl:set(Pool, Key, Exp, Value, transparent),
        ok
    catch
        _:_X ->
            {error, _X}
    end.

form_message(Type, Key, Timestamp) ->
    KeyBin = cbsweeper_utils:make_sure_binary(Key),
    TimestampBin = cbsweeper_utils:make_sure_binary(Timestamp),
    Content = << KeyBin/binary, "$", TimestampBin/binary >>,
    Length = size(Content),
    case Type of
        msg_queue ->
            <<1:8, Length:16, Content/binary>>;
        route_table ->
            <<2:8, Length:16, Content/binary>>;
        _Else ->
            ct:log("unknown type ~p for sweeper~n", [Type])
    end.

send_tcp_test_data(Socket, Type, Key, Timestamp) ->
    case gen_tcp:send(Socket, form_message(Type, Key, Timestamp)) of
        ok ->
            ct:log("Send data successfully~n");
        {error, Reason} ->
            ct:log("Send data failed because of ~p~n", [Reason])
    end.

is_key_exist_in_cb(Pool, Key) ->
    case cberl:get(Pool, Key) of
        {Key, {error, key_enoent}} ->
            false;
        _Else ->
            true
    end.

is_key_exist_in_ets(Ets, Key) ->
    case ets:lookup(Ets, Key) of
        [] ->
            false;
        _Else ->
            true
    end.
