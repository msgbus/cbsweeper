## Overview

定期清理 couchbase 过期 key，相当于一个外置 TTL 装置。

详细设计文档请参考：[couchbase_sweeper_desigin](https://bitbucket.org/msgbus/mbus_docs/wiki/couchbase_sweeper_desigin)

## How to run

`cbsweeper` 启动后，默认会监听 `6611` 端口， `elogic` 会连接这个端口。