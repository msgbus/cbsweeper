%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2015, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 03. 十一月 2015 下午9:50
%%%-------------------------------------------------------------------
-author("zhengyinyong").

-define(MSG_QUEUE_TIMESTAMP_INFO, msg_queue_timestamp_info).
-define(ROUTE_TABLE_TIMESTAMP_INFO, route_table_timestamp_info).

-define(MSG_QUEUE, 1).
-define(ROUTE_TABLE, 2).
-define(MSG_QUEUE_POOL,   msg_queue_pool).
-define(ROUTE_TABLE_POOL, route_table_pool).

-define(DEFAULT_MSG_QUEUE_POOL_ARGS, [
    {cb_host, "localhost"},
    {cb_username, "msg_queue"},
    {cb_bucket_name, "msg_queue"},
    {cb_bucket_password, "123456"},
    {cberl_pool_connection, 50}
]).

-define(DEFAULT_ROUTE_TABLE_POOL_ARGS, [
    {cb_host, "localhost"},
    {cb_username, "route_table"},
    {cb_bucket_name, "route_table"},
    {cb_bucket_password, "123456"},
    {cberl_pool_connection, 50}
]).

-define(MATCH_SPEC(Delta), [{{'$1','$2'},[{'=<','$2',Delta}],['$1']}]).
