%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2015, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 03. 十一月 2015 下午10:01
%%%-------------------------------------------------------------------
-module(cbsweeper_worker).
-author("zhengyinyong").

-compile({parse_transform, lager_transform}).
-include_lib("elog/include/elog.hrl").

-include("cbsweeper.hrl").

%% API
-export([sweep/1]).

sweep(RetainTime) ->
    ?INFO_MSG("sweeping now !"),
    Delta = cbsweeper_utils:get_millisec() - RetainTime,
    sweep_msg_queue_bucket(Delta),
    sweep_route_table_bucket(Delta).

%%%===================================================================
%%% Internal functions
%%%===================================================================
%% FIXME: 遍历 ets 表可能会耗时巨大
sweep_msg_queue_bucket(Delta) ->
    RetireKeys = ets:select(?MSG_QUEUE_TIMESTAMP_INFO, ?MATCH_SPEC(Delta)),
    case RetireKeys /= [] of
        true ->
            ?INFO("Delta = ~p, RetireKeys = ~p~n", [Delta, RetireKeys]),
            lists:foreach(fun(Key) -> cbsweeper_utils:cb_rm(?MSG_QUEUE_POOL, cbsweeper_utils:make_sure_binary(Key)),
                  ets:delete(?MSG_QUEUE_TIMESTAMP_INFO, Key) end, RetireKeys);
        false ->
            ignore
    end.

sweep_route_table_bucket(Delta) ->
    RetireKeys = ets:select(?ROUTE_TABLE_TIMESTAMP_INFO, ?MATCH_SPEC(Delta)),
    case RetireKeys /= [] of
        true ->
            ?INFO("Delta = ~p, RetireKeys = ~p~n", [Delta, RetireKeys]),
            lists:foreach(fun(Key) -> cbsweeper_utils:cb_rm(?ROUTE_TABLE_POOL, cbsweeper_utils:make_sure_binary(Key)),
                  ets:delete(?ROUTE_TABLE_TIMESTAMP_INFO, Key) end, RetireKeys);
        false ->
            ignore
    end.
