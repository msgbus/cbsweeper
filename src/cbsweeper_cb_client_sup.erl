%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2015, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 04. 十一月 2015 上午11:25
%%%-------------------------------------------------------------------
-module(cbsweeper_cb_client_sup).
-author("zhengyinyong").

-behaviour(supervisor).

%% API
-export([start_link/1]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%%
%% @end
%%--------------------------------------------------------------------
start_link(CBPoolArgs) ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, CBPoolArgs).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Whenever a supervisor is started using supervisor:start_link/[2,3],
%% this function is called by the new process to find out about
%% restart strategy, maximum restart frequency and child
%% specifications.
%%
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
    {ok, {SupFlags :: {RestartStrategy :: supervisor:strategy(),
        MaxR :: non_neg_integer(), MaxT :: non_neg_integer()},
        [ChildSpec :: supervisor:child_spec()]
    }} |
    ignore |
    {error, Reason :: term()}).
init(CBPoolArgs) ->
    RestartStrategy = one_for_one,
    MaxRestarts = 1000,
    MaxSecondsBetweenRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Restart = permanent,
    Shutdown = 2000,
    Type = worker,

    Children = [{cbpool_worker_name(PoolArgs), {cbsweeper_cb_client, start_link, [PoolArgs]},
        Restart, Shutdown, Type, [cbsweeper_cb_client]} || PoolArgs <- CBPoolArgs],

    {ok, {SupFlags, Children}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
cbpool_worker_name(PoolArgs) ->
    PoolName = proplists:get_value(cberl_pool_name, PoolArgs),
    list_to_atom(atom_to_list(PoolName) ++ "_worker").
