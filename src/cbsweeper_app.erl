-module(cbsweeper_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

-compile({parse_transform, lager_transform}).
-include_lib("elog/include/elog.hrl").

-include("cbsweeper.hrl").

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    start_listening(),
    create_ets(),
    CBPoolArgs = get_cb_pool_args(),
    start_sweep_timer(),
    cbsweeper_sup:start_link(CBPoolArgs).

stop(_State) ->
    ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================
start_listening() ->
    Port = application:get_env(cbsweeper, cbsweeper_listen_port, 6611),
    NbAcceptors = application:get_env(cbsweeper, cbsweeper_nbacceptors, 10000),
    {ok, _} = ranch:start_listener(cbsweeper, NbAcceptors,
                          ranch_tcp, [{port, Port}], cbsweeper_loop, []).

create_ets() ->
    ets:new(?MSG_QUEUE_TIMESTAMP_INFO, [ordered_set, public, named_table]),
    ets:new(?ROUTE_TABLE_TIMESTAMP_INFO, [ordered_set, public, named_table]).

get_cb_pool_args() ->
    [
        application:get_env(cbsweeper, cb_bucket_route_table, ?DEFAULT_ROUTE_TABLE_POOL_ARGS) ++ [{cberl_pool_name, ?ROUTE_TABLE_POOL}],
        application:get_env(cbsweeper, cb_bucket_msg_queue, ?DEFAULT_MSG_QUEUE_POOL_ARGS) ++ [{cberl_pool_name, ?MSG_QUEUE_POOL}]
    ].

start_sweep_timer() ->
    SweepInterval = application:get_env(cbsweeper, sweep_intervel, 5000),
    RetainTime = application:get_env(cbsweeper, retain_time, 5000),
    RetainTimeInMs = RetainTime * 60 * 1000,
    %% Todo: 数据量大的时候，耗时过长，会不会导致启动新的timer
    case timer:apply_interval(SweepInterval, cbsweeper_worker, sweep, [RetainTimeInMs]) of
        {ok, _TRef1} ->
            ?INFO("starting sweep timer for every ~p, retain time is ~p days~n", [SweepInterval, RetainTime]);
        {error, Reason} ->
            ?ERROR("starting sweep timer failed because of ~p~n", [Reason])
    end.
