%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2015, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 03. 十一月 2015 下午9:19
%%%-------------------------------------------------------------------
-module(cbsweeper_loop).
-author("zhengyinyong").

-export([start_link/4]).
-export([init/4]).

-behaviour(ranch_protocol).

-define(DEFAULT_TCP_BINARY_PROTOCOL_VERSION, 1).

-compile({parse_transform, lager_transform}).

-include("cbsweeper.hrl").
-include_lib("elog/include/elog.hrl").

start_link(Ref, Socket, Transport, Opts) ->
    Pid = spawn_link(?MODULE, init, [Ref, Socket, Transport, Opts]),
    {ok, Pid}.

init(Ref, Socket, Transport, Opts) ->
    ok = ranch:accept_ack(Ref),
    loop(Socket, Transport, Opts).

loop(Socket, Transport, Opts) ->
    case Transport:recv(Socket, 0, infinity) of
        {ok, Data} ->
            process_tcp_data(Data),
            loop(Socket, Transport, Opts);
        _ ->
            ignore
    end.

process_tcp_data(Data) ->
    <<Type:8, Length:16, KV:Length/binary, _Rest/binary>> = Data,
    case Type of
        ?MSG_QUEUE ->
            [Key, Timestamp] = binary:split(KV, <<"$">>),
            ?INFO("insert [~p:~p] in msg_queue_timestamp_info~n", [Key, Timestamp]),
            ets:insert(?MSG_QUEUE_TIMESTAMP_INFO, {Key, binary_to_integer(Timestamp)});
        ?ROUTE_TABLE ->
            [Key, Timestamp] = binary:split(KV, <<"$">>),
            ?INFO("insert [~p:~p] in route_table_timestamp_info~n", [Key, Timestamp]),
            ets:insert(?ROUTE_TABLE_TIMESTAMP_INFO, {Key, binary_to_integer(Timestamp)});
        false ->
            ?ERROR("illegal type: ~p~n", [Type]),
            ignore
    end.
