%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2015, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 04. 十一月 2015 上午11:55
%%%-------------------------------------------------------------------
-module(cbsweeper_sup).
-author("zhengyinyong").

-behaviour(supervisor).

-compile({parse_transform, lager_transform}).
-include_lib("elog/include/elog.hrl").

%% API
-export([start_link/1]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).
-define(CHILD2(I, Type, Args), {I, {I, start_link, [Args]}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================
start_link(CBPoolArgs) ->
    ?INFO("CBPoolArgs = ~p~n", [CBPoolArgs]),
    supervisor:start_link({local, ?MODULE}, ?MODULE, CBPoolArgs).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init(CBPoolArgs) ->
    {ok, { {one_for_one, 5, 10}, [?CHILD2(cbsweeper_cb_client_sup, supervisor, CBPoolArgs)]} }.
