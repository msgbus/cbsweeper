%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2015, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 05. 十一月 2015 下午3:38
%%%-------------------------------------------------------------------
-module(cbsweeper_utils).
-author("zhengyinyong").

-compile({parse_transform, lager_transform}).
-include_lib("elog/include/elog.hrl").

%% API
-export([get_millisec/0, make_sure_binary/1, cb_rm/2]).

get_millisec() ->
    {Mega, Sec, Micro} = os:timestamp(),
    (Mega*1000000 + Sec)*1000 + round(Micro/1000).

make_sure_binary(Data) ->
    if
        is_list(Data) ->
            list_to_binary(Data);
        is_integer(Data) ->
            integer_to_binary(Data);
        is_atom(Data) ->
            atom_to_binary(Data, latin1);
        true ->
            Data
    end.

cb_rm(Pool, Key) ->
    try
        ?INFO("remove ~p from ~p~n", [Key, Pool]),
        cberl:remove(Pool, Key)
    catch
        Type:Class ->
            ?ERROR("remove ~p in ~p failed, catch exception ~p:~p~n", [Key, Pool, Type, Class])
    end.
