#! /usr/bin/env escript

-module(send_tcp_test_data).
-author("zhengyinyong").

%% API
-export([start/2]).

-define(DEFAULT_TEST_HOST, "localhost").
-define(DEFAULT_TEST_PORT, 6611).

-define(MSG_QUEUE, 1).
-define(ROUTE_TABLE, 1).

-define(TestKey,       <<"msgq_2546343203789774592">>).
-define(TestTimestamp, <<"1446630468">>).

main(Args) ->
    case (Args /= []) andalso (length(Args) =:= 2) of
        true ->
            [Host, Port] = Args,
            start(Host, list_to_integer(Port));
        false ->
            io:format("./send_tcp_data.erl <host> <port>~n")
    end.

start(Host, Port) ->
    case gen_tcp:connect(Host, Port, [binary, {packet, 0}, {active,false}]) of
        {ok, Socket} ->
            io:format("CONNECT ~p:~p successfully~n", [Host, Port]),
            send_tcp_test_data(Socket),
            gen_tcp:close(Socket);
        {error, Reason} ->
            io:format("CONNECT ~p:~p failed beacause of ~p~n", [Host, Port, Reason])
    end.

send_tcp_test_data(Socket) ->
    case gen_tcp:send(Socket, form_message(msg_queue, ?TestKey, ?TestTimestamp)) of
        ok ->
            io:format("Send data successfully~n");
        {error, Reason} ->
            io:format("Send data failed because of ~p~n", [Reason])
    end.

form_message(Type, Key, Timestamp) ->
    Content = << Key/binary, "$", Timestamp/binary >>,
    Length = size(Content),
    case Type of
        msg_queue ->
            <<1:8, Length:16, Content/binary>>;
        route_table ->
            <<2:8, Length:16, Content/binary>>;
        _Else ->
            io:format("unknown type ~p for sweeper~n", [Type])
    end.

